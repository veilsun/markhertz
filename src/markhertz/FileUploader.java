package markhertz;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.ParentReference;
import com.intuit.quickbase.api.QuickBase;
import com.intuit.quickbase.api.QuickBaseConnection;
import com.intuit.quickbase.api.QuickBaseDatabase;
import com.intuit.quickbase.api.QuickBaseException;
import com.intuit.quickbase.api.QuickBaseRecord;
import com.intuit.quickbase.api.QuickBaseSimpleResultHandler;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static markhertz.MarkHertzProperties.*;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.io.FileUtils;
import veilsun.LogEmailHelper;

public class FileUploader {

    // Google Drive Service Account
    private static final String SERVICE_ACCOUNT_EMAIL = "drive-intergation-502@drive-integration-181215.iam.gserviceaccount.com";
    private static final String SERVICE_ACCOUNT_PKCS12_FILE_PATH = "Drive Integration-fe665f769419.p12";
    private static final String CLIENT_EMAIL = "admin@markhertzcompany.com";
    
    // QB Connection
    private static QuickBaseConnection qbc;
    private static QuickBaseDatabase db;
    
    // Local storage
    private static java.util.List<TrackDetail> trackDetails = new ArrayList<TrackDetail>();
    
    // Email helper
    private static String NOTIFY_ADMIN_USERS = "zstout@veilsun.com";
    private static LogEmailHelper logEmail = new LogEmailHelper("Mark Hertz - Cover Page File Uploader:");
    
    // Logger helper
    private static Logger logger = Logger.getLogger(FileUploader.class.getName());    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        logEmail.setReceipient(NOTIFY_ADMIN_USERS);
        logger.setLevel(Level.INFO);
        try {
            FileUploader t = new FileUploader();
            getTracks();
            
            if (trackDetails.isEmpty()) {
                logger.log(Level.INFO, "No new Track cover pages to upload");
                return;
            } else {
                logger.log(Level.INFO, "Processing records");
                t.uploadCoverPages();
            }
            
        } catch (URISyntaxException ex) {
            Logger.getLogger(FileUploader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (QuickBaseException ex) {
            Logger.getLogger(FileUploader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Throwable t) {
            Logger.getLogger(FileUploader.class.getName()).log(Level.SEVERE, null, t);
        }
        
        // Must be last line of main()
        logEmail.sendIfOutput();
    }
    
    public static void getTracks() throws URISyntaxException {
        try {
            // Setup QB connection
            qbc = QuickBase.getConnection(QB_APP_URL, QB_APP_TOKEN, QB_USER, QB_PASSWORD);
            db = new QuickBaseDatabase(qbc, QB_TRACKS_TABLE_ID, QB_APP_TOKEN);

            // Initialize and reset handler
            QuickBaseSimpleResultHandler handler = new QuickBaseSimpleResultHandler();
            handler.reset();

            // Get Projects where Google Folder Id is not blank and Cover Page Uploaded is not checked
            db.doQuery(handler, "{785.EX.0}AND{745.XEX.}AND{745.XEX.undefined}AND{773.XEX.}", "", "3.745.773");

            // Loop through records to create an ArrayList
            java.util.List<QuickBaseRecord> records = handler.getRecords();
                
            for (int i=0; i<records.size(); i++) {
                
                trackDetails.add(new TrackDetail(
                    records.get(i).getFieldById(3),
                    records.get(i).getFieldById(745),
                    records.get(i).getFieldById(773)
                ));
            }
            
        } catch (QuickBaseException e) {
            String error = e.getErrorText();
            logger.log(Level.WARNING, error);
            return;
        }
    }
    
    public static void updateTrack(TrackDetail trackDetail) {
        try {            
            List<NameValuePair> fieldData = new ArrayList<>();
            fieldData.add(new NameValuePair("785","1"));

            // Update with Lat, Long, and ReconId
            db.editRecord(trackDetail.recordId, fieldData);
            
        } catch (QuickBaseException e) {
            String error = e.getErrorText();
            logger.log(Level.WARNING, error);
            return;
        }
    }
    
    private void uploadCoverPages() throws QuickBaseException{
        File uploadObj;
        
        try {
            Drive drive = getDriveService();
            
            for (TrackDetail trackDetail : trackDetails) {
                uploadObj = insertFile(drive, trackDetail.googleFolderId,
                        "", trackDetail.filename, qbc.executeForStream(trackDetail.fileUrl)); 
                if (uploadObj != null) { 
                    updateTrack(trackDetail);
                }
            }
            
        } catch (GeneralSecurityException ex) {
            Logger.getLogger(FileUploader.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(FileUploader.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public static Drive getDriveService() throws GeneralSecurityException,
            IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        JacksonFactory jsonFactory = new JacksonFactory();
        GoogleCredential credential = new GoogleCredential.Builder()
            .setTransport(httpTransport)
            .setJsonFactory(jsonFactory)
            .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
            .setServiceAccountPrivateKeyFromP12File(new java.io.File(SERVICE_ACCOUNT_PKCS12_FILE_PATH))
            .setServiceAccountScopes(Collections.singleton(DriveScopes.DRIVE))
            .setServiceAccountUser(CLIENT_EMAIL)
            .build();
        
        Drive service = new Drive.Builder(httpTransport, jsonFactory, null)
                .setHttpRequestInitializer(credential)
                .setApplicationName("Drive Integration")
                .build();
        return service;
    }
    
    private static File insertFile(Drive service, String parentId, 
            String mimeType, String filename, InputStream stream) throws IOException {
        
        // File's metadata.
        File body = new File();
        body.setTitle(filename);
        body.setDescription("");
        body.setMimeType(URLConnection.guessContentTypeFromStream(stream));

        // Set the parent folder.
        if (parentId != null && parentId.length() > 0) {
            body.setParents(
                Arrays.asList(new ParentReference().setId(parentId)));
        }

        InputStreamContent mediaContent = new InputStreamContent(mimeType, new BufferedInputStream(stream));  
        try {
            File file = service.files().insert(body, mediaContent).execute();

            return file;
        } catch (IOException e) {
            logger.log(Level.WARNING, "Service Error: "+ e);
            return null;
        }
    }
}

class TrackDetail {
    String recordId;
    String googleFolderId;
    String filename;
    String fileUrl;

    public TrackDetail(String recordId, String googleFolderId, String fileAttachment) {
        this.recordId = recordId;
        this.googleFolderId = googleFolderId;
        this.filename = fileAttachment.substring(0, fileAttachment.indexOf("<url>"));
        this.fileUrl = fileAttachment.substring(fileAttachment.indexOf("<url>") + "<url>".length(),fileAttachment.indexOf("</url>"));
    }
}


